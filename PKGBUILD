# Maintainer: Danct12 <danct12@disroot.org>
# Contributor: Kevin MacMartin <prurigro@gmail.com>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgname=evince-mobile
pkgver=46.3
pkgrel=1
pkgdesc="Document viewer (PDF, PostScript, XPS, djvu, dvi, tiff, cbr, cbz, cb7, cbt)"
arch=('aarch64' 'x86_64')
url="https://apps.gnome.org/Evince"
license=(GPL-2.0-or-later)
depends=(
  at-spi2-core
  cairo
  dconf
  djvulibre
  gcc-libs
  gdk-pixbuf2
  glib2
  glibc
  gnome-desktop
  gsettings-desktop-schemas
  gsfonts
  gspell
  gst-plugins-base-libs
  gstreamer
  gtk3
  gvfs
  hicolor-icon-theme
  libarchive
  libgxps
  libhandy
  libsecret
  libspectre
  libsynctex
  libtiff
  libxml2
  pango
  poppler-glib
)
makedepends=(
  appstream-glib
  git
  glib2-devel
  gobject-introspection
  meson
  texlive-bin
)
source=("git+https://gitlab.gnome.org/GNOME/evince.git?signed#tag=${pkgver/[a-z]/.&}"
        # UI patches from Purism
        '0001-toolbar-Allow-to-toggle-the-sidebar-button.patch'
        '0002-toolbar-Allow-hiding-zoom-annotation-and-page-widgets.patch'
        '0003-properties-view-Reduce-the-label-width-chars.patch'
        '0004-window-Port-it-to-phones.patch')
b2sums=('f0a6c569a6175c433c0ae0cf77fe5f0b819959a9199d35b5442d6f8e6c663ad83663bd277f368581f5d2400c124896b2c13924c697e5c2b06faaef0469f99bdb'
        '8d98221fc48a578f9a22c31c443944ff2d1fbf579107d493b18937689a31d4ad4a49c577fe8cfaa46d075c771776698bc68b50b064207fe49b99163a70c54543'
        '8c4f884c240af0e5eead4a2e0cbc77f31f26c30e83d4ff110660086197a337b3e4255362622ee0023850ce3f519f091f5c2a8af1f9eaab00df2c372c0bf4375c'
        '118645c39de006e601e0d3406b69f737347efd4a6a415b0a5149527b22bc4d870c11479d5538d559b6f3c234cbc593701c737c94bd7027577930b925ae01f16f'
        'f060eebcc1877ee1f5825697553d7ab1b99a1d99a71f442ca38b1fcfd918e1064518281ce51cc27e6b265244e3b0de31066da70ee1bf9f8041cb08109d332538')
validpgpkeys=('6F3E1831D69760DC3FCE7873D6197451C129658C') # Germán Poo-Caamaño <gpoo@gnome.org>

pkgver() {
  cd evince
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd evince

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  local meson_options=(
    -D ps=enabled
    -D gtk_doc=false
    -D user_doc=false
  )

  arch-meson evince build "${meson_options[@]}"
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  provides=("evince=$pkgver" libev{document,view}3.so)
  conflicts=('evince')
  optdepends=('texlive-bin: DVI support')
  

  meson install -C build --destdir "$pkgdir"
}
